import random


def bubble_sort(nums):
    for i in range(len(nums)):  # run the iteration length times
        for j in range(len(nums)-1-i):
            if nums[j] > nums[j + 1]:
                nums[j], nums[j+1] = nums[j+1], nums[j]
    return nums


nums = [random.randint(1, 100) for n in range(10)]
# a list of 100000 random numbers in the range of 1 to 1 million

a = 9
b = 10

# swap now
a, b = b, a # swapping (Python ONLY)
print(bubble_sort(nums))