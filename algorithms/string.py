"""
I  like     Python. {list of characters}
=> I like Python

space_added = False
"""


def trim_extra_space(sentence):
    new_string = ""
    space_added = False
    for char in sentence:
        if char == " ":
            if not space_added:
                new_string += char  # add a space
                space_added = True
        else:
            new_string += char
            space_added = False
    return new_string


print(trim_extra_space("I     like   Python    very     very much"))
