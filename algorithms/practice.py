import random as rd

def is_triangular(n):
    tri = 0
    for i in range(1, n+1):
        tri += i
        if tri > n:
            return False
        elif tri == n:
            return True
    return False

def odd_one_out(nums):
    freq_map = {}
    for num in nums:
        if num in freq_map:
            freq_map[num] += 1
        else:
            freq_map[num] = 1
    for key in freq_map:
        if freq_map[key] == 1:
            return key


def odd_one_out_xor(nums):
    pass


def guess_game():
    prize = rd.randint(1, 5)
    attempts = 0
    guess = -1
    max_attempts = 3
    while guess != prize and attempts < max_attempts:
        guess = int(input("enter your guess\t"))
        attempts += 1

    if guess == prize:
        return f"Winner. Attempts taken {attempts}. Correct answer was {prize}"
    else:
        return f"Hard Luck. The correct answer was {prize}. You took {attempts} attempts"


def print_digits(n):
    # prints the digits of n backwards (without using string/ list approach)
    while n > 0:
        print(n % 10)
        n = n // 10


"""
[8, 10, 13, 16, 20, 23, 25]
search = 26 -> TRUE
"""


def pairwise_sum_n2(nums, search):
    found = False
    for i in range(len(nums)):
        for j in range(len(nums)):
            if nums[i] + nums[j] == search:
                found = True
                break
    return found


# negative indexing
def pairwise_sum(nums, search):
    low = 0
    high = len(nums)-1
    while high >= low:
        if nums[low] + nums[high] == search:
            return True
        elif nums[low] + nums[high] < search:
            low += 1
        else:
            high -= 1
    return False


def insert_correctly(nums, key):
    inserted = False
    for i in range(len(nums)):
        if nums[i] > key:
            nums.insert(i, key)
            inserted = True
            break
    if not inserted:
        nums.append(key)
    return nums


def insert_correctly_2(nums, key):
    to_be_moved = 0
    for i in range(len(nums)):
        if nums[i] > key:
            to_be_moved = len(nums) - i
            break
    nums.append(0)
    for i in range(0, to_be_moved):
        nums[len(nums)-1-i] = nums[len(nums)-1-i-1]
    nums[len(nums)-to_be_moved-1] = key
    return nums


def is_narcissitic(num):
    exp = len(str(num))
    num = str(num)
    value = 0
    for i in range(exp):
        value += int(num[i])**exp
    return value == int(num)

print(is_narcissitic(548834))

