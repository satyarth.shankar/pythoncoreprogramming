def divide(a, b):
    try:
        file = open("conditionals1.py", "r")
        file.close()
        print(a / b)
    except ZeroDivisionError:
        # "Handling the exception"
        print("can't divide by zero")
    except TypeError:
        print("wrong data type received")
    except FileNotFoundError:
        print("file does not exist")


def divide2(a, b):
    try:
        print(a / b)
    except:
        print("some exception occurred")
    else:
        print("everything went along smoothly")
    finally:
        # ALWAYS gets triggered
        print("coming out of the function")


print("Before Exception")
divide2("String", 0)
print("After Exception")