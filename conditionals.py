def myfunc():
    print("hey")
    print("I")
    print("like")
    print("Python")


def if_else_func():
    num = int(input("enter number:\t"))
    if num % 2 == 0:
        print("even number")
    else:
        print("odd number")


admins = ("George", "Muhammad", "Sugar", "Manny", "Anthony")

def name_check(search):
    if search in admins:    #membership test (tuples, lists, dictonaries)
        print("an admin exists")
    else:
        print("no such record")


def final_bill(amount, membership):
    """
    This function calculates the final bill after applying membership discounts
    :param amount: initial bill
    :param membership: membership type
    :return:
    """
    if membership == "Gold":
        amount *= 0.9
    elif membership == "Silver":
        amount *= 0.95
    elif membership == "Diamond":
        amount *= 0.85
    else:
        pass
    print(amount)


def divisible_by_6(x):
    # demonstrates nested conditionals
    if x % 2 == 0:
        if x % 3 == 0:
            print("number is divisible by 6")
        else:
            print("divisible by 2, not by 3")
    else:
        if x % 3 == 0:
            print("divisible by 3, not by 2")
        else:
            print("divisible by neither 2 nor 3 ")


# at least six characters, not entirely numeric
def password_validator(password):
    # demonstrates compound conditional
    valid_len = len(password) >= 6
    valid_numeric = not password.isnumeric()
    if valid_len and valid_numeric:
        print("valid password")
    else:
        print("invalid password")


password = input("enter password:\t")
password_validator(password)













