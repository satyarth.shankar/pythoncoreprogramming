import csv

guests = ["Alex", "Bob", "Carlos", "Don", "Elena", "Farooq"]

def create_file():
    file = open("guests.txt", "w")
    for guest in guests:
        file.write(guest + "\n")
    file.close()   # DO NOT FORGET


def create_file_csv():
    file = open("guests.csv", "w")
    writer = csv.writer(file)
    for guest in guests:
        writer.writerow([guest])
    file.close()

results = {
    "A": 90,
    "B": 100,
    "C": 80,
    "D": 90,
    "E": 100,
}


def store_results():
    file = open("results.csv", "w")
    writer = csv.writer(file)
    for key in results:
        writer.writerow([key, results[key]])
    file.close()
store_results()



