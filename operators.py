# user input

# height = int(input("How tall are you in cm?"))
# print("Your height was received as " + str(height))

x = 9
y = 4

# arithmetic operations
print(x + y)
print(x - y)
print(x * y)
print(x / y)
print(x // y)   # quotient of dividing x by y
print(x % y)   # x mod y : REMAINDER when you divided x by y
print(x ** y)  # x to the power y

a = "Hi"
b = "Bye"

print(a + b)
print(a + b*4)


print("--------------")

# relational operators
print(x > y)
print(x < y)
print(x >= y)
print(x <= y)


print("............")


# logical operators
c = True
d = False

print(c or d)   # true if at least one is True  {UNION}
print(c and d)  # true if both are true {INTERSECTION}
print(not c)    # {COMPLEMENT}
print(not d)

# binary operators
print("*************************")
x = 6
y = 3
print(x & y)    # binary AND
print(x | y)    # binary OR
print(x ^ y)    # binary XOR
