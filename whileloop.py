def print10():
    """
    prints the first 10 natural numbers
    :return: None
    """
    i = 1
    while i <= 10:
        print(i)
        i += 1 # do not forgot this step


# at least six characters, not entirely numeric
def password_validator(password):
    # demonstrates compound conditional
    valid_len = len(password) >= 6
    valid_numeric = not password.isnumeric()
    if valid_len and valid_numeric:
        return True
    else:
        return False


def get_new_password():
    password = input("enter password:\t")
    result = password_validator(password)
    while result == False:
        print("Invalid Password")
        password = input("enter password:\t")
        result = password_validator(password)
    print("Valid Password")


get_new_password()


