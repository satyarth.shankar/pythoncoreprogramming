def no_of_moves(position):
    moves = 0
    left = position % 8
    right = 7 - position % 8
    bottom = position // 8
    top = 7 - position // 8
    moves += quarter_score(x_margin=left, y_margin=top)
    moves += quarter_score(x_margin=left, y_margin=bottom)
    moves += quarter_score(x_margin=right, y_margin=top)
    moves += quarter_score(x_margin=right, y_margin=bottom)
    print(moves)


def quarter_score(x_margin, y_margin):
    high = mid = low = 0
    if x_margin > 1:
        high += 1
    elif x_margin == 1:
        mid += 1
    else:
        low += 1

    if y_margin > 1:
        high += 1
    elif y_margin == 1:
        mid += 1
    else:
        low += 1

    return max(high - 2*low, 0)


def target_page(loggedIn, member):
    if loggedIn:
        if member:
            print("ContentHome.html")
        else:
            print("MembershipOptions.html")
    else:
        if not member:
            print("LoginOrSignUp.html")
        else:
            pass #invalid case


def award_points(word):
    points = 0
    if isinstance(word, str):
        word = word.replace(" ", "")
        points += len(word) * 10
        word = word.lower()
        if word.startswith("a") or \
            word.startswith("e") or \
            word.startswith("i") or \
            word.startswith("o") or \
            word.startswith("u"):
            points += 5
        else:
            points -= 2

        if word.endswith("a") or \
            word.endswith("e") or \
            word.endswith("i") or \
            word.endswith("o") or \
            word.endswith("u"):
            points -= 2
        else:
            points += 5
    print(points)



