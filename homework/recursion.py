import math

def unit_digit(base, exp):
    if exp == 0:
        return 1
    else:
        return (base*unit_digit(base, exp-1)) % 10


def print_chars(string):
    if len(string) == 1:
        print(string[0])
        return
    print(string[0], end="-")
    print_chars(string[1:])


def sum_gp(a, r, n):
    if n == 1:
        return a
    else:
        return math.pow(r, n-1)*a + sum_gp(a, r, n-1)


def all_true(bools):
    if len(bools) == 1:
        return bools[0]
    else:
        return bools[0] and all_true(bools[1:])


def at_least_one_true(bools):
    if len(bools) == 1:
        return bools[0]
    return bools[0] or at_least_one_true(bools[1:])


sum_lemma = lambda n : n*(n+1)/2


def sum_func(n):
    total = 0
    for i in range(1, n+1):
        total += i
    return total


def test_sum_lemma(sum_lemma, limit):
    if limit == 1:
        return sum_lemma(limit) == sum_func(limit)
    else:
        return sum_lemma(limit) == sum_func(limit) and test_sum_lemma(sum_lemma, limit-1)



