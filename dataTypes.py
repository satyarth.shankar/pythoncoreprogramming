x = 8   # integer
y = "Hi"    # string
t = True    # Boolean
v = 2.718   # float

a = ["Eggs", "Honey", "Bread"]
a.append("Chocolates")
b = ("Eggs", "Honey")   # immutable

c = {
    "Eggs": 50,
    "Honey": 100,
    "Bread": 55
}
c["Eggs"] = 60
c["Jam"] = 90
print(c)

