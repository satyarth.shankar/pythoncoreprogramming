def exp(x):
    return 2.718 ** x


def print10():
    for i in range(1, 11, 1):
        """
        start : default = 0, inclusive
        stop : exclusive
        step : default = 1
        """
        print(i)


def ap(a, d, n):
    """
    print the first n terms of the AP
    :param a: first
    :param d: common difference
    :param n: number of terms
    :return: None
    """

    for i in range(1, n+1, 1):
        print(a + (i-1) * d)


guests = ["Alex", "Bob", "Carlos", "Don", "Elena", "Farooq"]


def print_guest_list():
    for i in range(len(guests)):
        print(guests[i])


def check_guest(name):
    found = False
    for i in range(len(guests)):
        if guests[i] == name:
            found = True
            break   # stop the loop as we don't need to look anymore
    return found


def receiver():
    name = input("enter name\t")
    if check_guest(name):
        print("you are welcome")
    else:
        response = input("Would you like to be added? Y or N\t")
        if response[0].upper() == "Y":
            guests.append(name)
            print("Updated Guest List:")
            print_guest_list()
        else:
            print("Thank you for your time")



results = {
    "A": 90,
    "B": 100,
    "C": 80,
    "D": 90,
    "E": 100,
}

def get_names():
    for key in results:
        # print(key, results.get(key))  #method based
        print(key, results[key])    #accessor based


def average():
    total = 0
    for key in results:
        total += results[key]
    avg = total / len(results)
    print(avg)










