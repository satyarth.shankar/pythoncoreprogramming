class Charity():

    def __init__(self):
        self.donations = {}

    def donate(self, donor, amount):
        if amount > 0:
            if donor in self.donations:
                self.donations[donor].append(amount)
            else:
                self.donations[donor] = [amount]    # initialisation

    def donors(self):
        return list(self.donations.keys())

    def get_my_donations(self, donor):
        return self.donations.get(donor)


def runner():
    myCharity = Charity()
    myCharity.donate("Satyarth", 9000)
    myCharity.donate("Satyarth", 6000)
    myCharity.donate("Ted", 8000)
    print(myCharity.get_my_donations("Satyarth"))

runner()
