class Taxi():

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.distance = 0
        self.rate_per_km = 25

    def get_pos(self):
        return self.x, self.y

    def get_distance(self):
        return self.distance

    def get_fare(self):
        return self.distance * self.rate_per_km

    def move_x(self, x_dist):
        self.x += x_dist
        self.distance += abs(x_dist)

    def move_y(self, y_dist):
        self.y += y_dist
        self.distance += abs(y_dist)


def runner():
    myTaxi = Taxi(2, 1)
    myTaxi.move_x(5)
    myTaxi.move_y(2)
    myTaxi.move_x(-3)

    print(myTaxi.get_pos())
    print(myTaxi.get_distance())
    print(myTaxi.get_fare())

runner()

