class Fruit():

    def __init__(self, name, color, season, price):
        self.name = name
        self.color = color
        self.seasonal = season
        self.price = price

    def details(self):
        season_string = "This is seasonal." if self.seasonal else "This is not seasonal."
        print(f"A {self.color} fruit which is called {self.name}\n{season_string}")




