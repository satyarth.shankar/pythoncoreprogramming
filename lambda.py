my_func = lambda x, y: x**y
get_full_name = lambda fn, ln: fn + " " + ln


def introduce(full, age):
    full_name = full("Alex", "Carey")
    # f string formatting
    print(f"This is {full_name} aged {age} years")

