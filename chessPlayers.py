import csv

scoreboard = {}

file = open("Chess.csv", "r")
reader = csv.reader(file)

next(reader)
for line in reader:
    if line[1] not in scoreboard:
        scoreboard[line[1]] = [int(line[2]), 1]
    else:
        scoreboard[line[1]][0] += int(line[2])
        scoreboard[line[1]][1] += 1

file.close()

avg_score_board = {}
for key in scoreboard:
    avg_score_board[key] = round(scoreboard[key][0]/ scoreboard[key][1], 2)
print(avg_score_board)
# HW : plot the average score for on a bar chart (matplotlib)