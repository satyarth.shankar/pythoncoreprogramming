import matplotlib.pyplot as mplot


def plot_square(n):
    x = []
    y = []
    for i in range(n):
        x.append(i)
        y.append(i*i)
    mplot.title("y vs x^2")
    mplot.xlabel("x->")
    mplot.ylabel("y->")
    mplot.plot(x, y)
    mplot.show()


def list_comprehension():
    y = [i**3 for i in range(1, 11)]
    return y


def matrix():
    m = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ]
    return m


def prob_matrix(mat):
    for row in mat:
        total = sum(row)
        for i in range(len(row)):
            row[i] = round(row[i] / total, 3)
    return mat


def starts_with_vowel(string):
    vowels = ("a", "e", "i", "o", "u")
    if isinstance(string, str):
        if string.lower()[0] in vowels: # membership test
            return True
        return False
    return None


def get_results_dict():
    students = ["A", "B", "C", "D", "E"]
    marks = [87, 90, 76, 96, 83]
    result = {}
    for i in range(len(students)):
        result[students[i]] = marks[i]
    return result


def get_topper(result):
    high = -999
    high_scorer = None
    for key in result:
        if result[key] > high:
            high = result[key]
            high_scorer = key
    return high_scorer


def char_freq(string):
    string = string.lower()
    freq_map = {}
    for letter in string:
        if letter in freq_map:  # membership test
            freq_map[letter] += 1
        else:
            freq_map[letter] = 1
    return freq_map










