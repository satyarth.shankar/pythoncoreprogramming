x = 5
y = ["A", "B"]
# global scope

def my_func():
    # local scope
    x = 10
    y.append("C")

my_func()
print(x)
print(y)