def factorial_iter(n):
    prod = 1
    for i in range(1, n+1):
        prod *= i
    return prod


def factorial_recur(n):
    if n == 0:
        return 1
    else:
        return n * factorial_recur(n-1)


def list_total(nums):
    if len(nums) == 1:
        return nums[0]
    else:
        return nums[0] + list_total(nums[1:])


def power(base, exponent):
    if exponent < 0:
        return 1 / power(base, -exponent)
    if exponent == 0:
        return 1
    return base * power(base, exponent-1)


def all_true(myList):
    """
    :param myList: list of booleans
    :return: True if all values in the list are true
    """
    if len(myList) == 1:
        return myList[0]
    else:
        return myList[0] and all_true(myList[1:])

print(all_true([True, True, True]))
